/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import * as __model from '../model';

export interface ContactParams {
  data: __model.ContactUs;
}

@Injectable()
export class CarefieSettingsService {
  constructor(private http: HttpClient) {}

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_settings/carefie_settings_category_list */
  category(): Observable<__model.CampaignCategory[]> {
    return this.http.get<__model.CampaignCategory[]>(`/api/v1/carefie_settings/category/`);
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_settings/carefie_settings_contact_create */
  contact(params: ContactParams): Observable<__model.ContactUs> {
    const bodyParams = params.data;

    return this.http.post<__model.ContactUs>(`/api/v1/carefie_settings/contact/`, bodyParams || {});
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_settings/carefie_settings_faq_list */
  faq(): Observable<__model.Category[]> {
    return this.http.get<__model.Category[]>(`/api/v1/carefie_settings/faq/`);
  }
}
