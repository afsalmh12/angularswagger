/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import * as __model from '../model';

export interface AuthParams {
  data: __model.AuthToken;
}

export interface ForgotPasswordParams {
  data: __model.ForgotPassword;
}

export interface CarefieAccountsMeUpdateParams {
  data: __model.User;
}

export interface CarefieAccountsMePartialUpdateParams {
  data: __model.User;
}

export interface ResetPasswordParams {
  data: __model.ResetPassword;
}

export interface CarefieAccountsUsersCreateParams {
  data: __model.User;
}

export interface CarefieAccountsUsersReadParams {
  /** A unique integer value identifying this user. */
  id: number;
}

export interface CarefieAccountsUsersUpdateParams {
  data: __model.User;
  /** A unique integer value identifying this user. */
  id: number;
}

export interface CarefieAccountsUsersPartialUpdateParams {
  data: __model.User;
  /** A unique integer value identifying this user. */
  id: number;
}

export interface CarefieAccountsUsersDeleteParams {
  /** A unique integer value identifying this user. */
  id: number;
}

@Injectable()
export class CarefieAccountsService {
  constructor(private http: HttpClient) {}

  /**
   * Request token for user authentication
   * http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_accounts/carefie_accounts_auth_create
   */
  auth(params: AuthParams): Observable<__model.AuthToken> {
    const bodyParams = params.data;

    return this.http.post<__model.AuthToken>(`/api/v1/carefie_accounts/auth/`, bodyParams || {});
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_accounts/carefie_accounts_forgot-password_create */
  forgotPassword(params: ForgotPasswordParams): Observable<__model.ForgotPassword> {
    const bodyParams = params.data;

    return this.http.post<__model.ForgotPassword>(`/api/v1/carefie_accounts/forgot-password/`, bodyParams || {});
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_accounts/carefie_accounts_me_read */
  carefieAccountsMeRead(): Observable<__model.User> {
    return this.http.get<__model.User>(`/api/v1/carefie_accounts/me/`);
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_accounts/carefie_accounts_me_update */
  carefieAccountsMeUpdate(params: CarefieAccountsMeUpdateParams): Observable<__model.User> {
    const bodyParams = params.data;

    return this.http.put<__model.User>(`/api/v1/carefie_accounts/me/`, bodyParams || {});
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_accounts/carefie_accounts_me_partial_update */
  carefieAccountsMePartialUpdate(params: CarefieAccountsMePartialUpdateParams): Observable<__model.User> {
    const bodyParams = params.data;

    return this.http.patch<__model.User>(`/api/v1/carefie_accounts/me/`, bodyParams || {});
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_accounts/carefie_accounts_reset-password_create */
  resetPassword(params: ResetPasswordParams): Observable<__model.ResetPassword> {
    const bodyParams = params.data;

    return this.http.post<__model.ResetPassword>(`/api/v1/carefie_accounts/reset-password/`, bodyParams || {});
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_accounts/carefie_accounts_users_list */
  carefieAccountsUsersList(): Observable<__model.User[]> {
    return this.http.get<__model.User[]>(`/api/v1/carefie_accounts/users/`);
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_accounts/carefie_accounts_users_create */
  carefieAccountsUsersCreate(params: CarefieAccountsUsersCreateParams): Observable<__model.User> {
    const bodyParams = params.data;

    return this.http.post<__model.User>(`/api/v1/carefie_accounts/users/`, bodyParams || {});
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_accounts/carefie_accounts_users_read */
  carefieAccountsUsersRead(params: CarefieAccountsUsersReadParams): Observable<__model.User> {
    const pathParams = {
      id: params.id,
    };
    return this.http.get<__model.User>(`/api/v1/carefie_accounts/users/${pathParams.id}/`);
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_accounts/carefie_accounts_users_update */
  carefieAccountsUsersUpdate(params: CarefieAccountsUsersUpdateParams): Observable<__model.User> {
    const bodyParams = params.data;

    const pathParams = {
      id: params.id,
    };
    return this.http.put<__model.User>(`/api/v1/carefie_accounts/users/${pathParams.id}/`, bodyParams || {});
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_accounts/carefie_accounts_users_partial_update */
  carefieAccountsUsersPartialUpdate(params: CarefieAccountsUsersPartialUpdateParams): Observable<__model.User> {
    const bodyParams = params.data;

    const pathParams = {
      id: params.id,
    };
    return this.http.patch<__model.User>(`/api/v1/carefie_accounts/users/${pathParams.id}/`, bodyParams || {});
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_accounts/carefie_accounts_users_delete */
  carefieAccountsUsersDelete(params: CarefieAccountsUsersDeleteParams): Observable<void> {
    const pathParams = {
      id: params.id,
    };
    return this.http.delete<void>(`/api/v1/carefie_accounts/users/${pathParams.id}/`);
  }
}
