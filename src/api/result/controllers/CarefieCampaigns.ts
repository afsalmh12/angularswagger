/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

import * as __model from '../model';

export interface CarefieCampaignsCampaignsCreateParams {
  data: __model.CampaignCreation;
}

export interface CarefieCampaignsCampaignsBankDocumentsListParams {
  campaign_id: string;
}

export interface CarefieCampaignsCampaignsBankDocumentsCreateParams {
  data: __model.BankDocumentCreate;
  campaign_id: string;
}

export interface CarefieCampaignsCampaignsBankDocumentsReadParams {
  campaign_id: string;
  id: string;
}

export interface CarefieCampaignsCampaignsBankDocumentsUpdateParams {
  data: __model.BankDocumentCreate;
  campaign_id: string;
  id: string;
}

export interface CarefieCampaignsCampaignsBankDocumentsPartialUpdateParams {
  data: __model.BankDocumentCreate;
  campaign_id: string;
  id: string;
}

export interface CarefieCampaignsCampaignsBankDocumentsDeleteParams {
  campaign_id: string;
  id: string;
}

export interface CarefieCampaignsCampaignsCampaignImagesListParams {
  campaign_id: string;
}

export interface CarefieCampaignsCampaignsCampaignImagesCreateParams {
  data: __model.CampaignImageCreate;
  campaign_id: string;
}

export interface CarefieCampaignsCampaignsCampaignImagesReadParams {
  campaign_id: string;
  id: string;
}

export interface CarefieCampaignsCampaignsCampaignImagesUpdateParams {
  data: __model.CampaignImageCreate;
  campaign_id: string;
  id: string;
}

export interface CarefieCampaignsCampaignsCampaignImagesPartialUpdateParams {
  data: __model.CampaignImageCreate;
  campaign_id: string;
  id: string;
}

export interface CarefieCampaignsCampaignsCampaignImagesDeleteParams {
  campaign_id: string;
  id: string;
}

export interface CarefieCampaignsCampaignsOrganisationDocumentsListParams {
  campaign_id: string;
}

export interface CarefieCampaignsCampaignsOrganisationDocumentsCreateParams {
  data: __model.OrganisationDocumentCreate;
  campaign_id: string;
}

export interface CarefieCampaignsCampaignsOrganisationDocumentsReadParams {
  campaign_id: string;
  id: string;
}

export interface CarefieCampaignsCampaignsOrganisationDocumentsUpdateParams {
  data: __model.OrganisationDocumentCreate;
  campaign_id: string;
  id: string;
}

export interface CarefieCampaignsCampaignsOrganisationDocumentsPartialUpdateParams {
  data: __model.OrganisationDocumentCreate;
  campaign_id: string;
  id: string;
}

export interface CarefieCampaignsCampaignsOrganisationDocumentsDeleteParams {
  campaign_id: string;
  id: string;
}

export interface CarefieCampaignsCampaignsReadParams {
  id: string;
}

export interface CarefieCampaignsCampaignsUpdateParams {
  data: __model.CampaignCreation;
  id: string;
}

export interface CarefieCampaignsCampaignsPartialUpdateParams {
  data: __model.CampaignCreation;
  id: string;
}

export interface CarefieCampaignsCampaignsDeleteParams {
  id: string;
}

@Injectable()
export class CarefieCampaignsService {
  constructor(private http: HttpClient) {}

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_list */
  carefieCampaignsCampaignsList(): Observable<__model.CampaignCreation[]> {
    return this.http.get<__model.CampaignCreation[]>(`/api/v1/carefie_campaigns/campaigns/`);
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_create */
  carefieCampaignsCampaignsCreate(params: CarefieCampaignsCampaignsCreateParams): Observable<__model.CampaignCreation> {
    const bodyParams = params.data;

    return this.http.post<__model.CampaignCreation>(`/api/v1/carefie_campaigns/campaigns/`, bodyParams || {});
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_bank-documents_list */
  carefieCampaignsCampaignsBankDocumentsList(params: CarefieCampaignsCampaignsBankDocumentsListParams): Observable<__model.BankDocumentRead[]> {
    const pathParams = {
      campaign_id: params.campaign_id,
    };
    return this.http.get<__model.BankDocumentRead[]>(`/api/v1/carefie_campaigns/campaigns/${pathParams.campaign_id}/bank-documents/`);
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_bank-documents_create */
  carefieCampaignsCampaignsBankDocumentsCreate(params: CarefieCampaignsCampaignsBankDocumentsCreateParams): Observable<__model.BankDocumentCreate> {
    const bodyParams = params.data;

    const pathParams = {
      campaign_id: params.campaign_id,
    };
    return this.http.post<__model.BankDocumentCreate>(`/api/v1/carefie_campaigns/campaigns/${pathParams.campaign_id}/bank-documents/`, bodyParams || {});
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_bank-documents_read */
  carefieCampaignsCampaignsBankDocumentsRead(params: CarefieCampaignsCampaignsBankDocumentsReadParams): Observable<__model.BankDocumentRead> {
    const pathParams = {
      campaign_id: params.campaign_id,
      id: params.id,
    };
    return this.http.get<__model.BankDocumentRead>(`/api/v1/carefie_campaigns/campaigns/${pathParams.campaign_id}/bank-documents/${pathParams.id}/`);
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_bank-documents_update */
  carefieCampaignsCampaignsBankDocumentsUpdate(params: CarefieCampaignsCampaignsBankDocumentsUpdateParams): Observable<__model.BankDocumentCreate> {
    const bodyParams = params.data;

    const pathParams = {
      campaign_id: params.campaign_id,
      id: params.id,
    };
    return this.http.put<__model.BankDocumentCreate>(`/api/v1/carefie_campaigns/campaigns/${pathParams.campaign_id}/bank-documents/${pathParams.id}/`, bodyParams || {});
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_bank-documents_partial_update */
  carefieCampaignsCampaignsBankDocumentsPartialUpdate(params: CarefieCampaignsCampaignsBankDocumentsPartialUpdateParams): Observable<__model.BankDocumentCreate> {
    const bodyParams = params.data;

    const pathParams = {
      campaign_id: params.campaign_id,
      id: params.id,
    };
    return this.http.patch<__model.BankDocumentCreate>(`/api/v1/carefie_campaigns/campaigns/${pathParams.campaign_id}/bank-documents/${pathParams.id}/`, bodyParams || {});
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_bank-documents_delete */
  carefieCampaignsCampaignsBankDocumentsDelete(params: CarefieCampaignsCampaignsBankDocumentsDeleteParams): Observable<void> {
    const pathParams = {
      campaign_id: params.campaign_id,
      id: params.id,
    };
    return this.http.delete<void>(`/api/v1/carefie_campaigns/campaigns/${pathParams.campaign_id}/bank-documents/${pathParams.id}/`);
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_campaign-images_list */
  carefieCampaignsCampaignsCampaignImagesList(params: CarefieCampaignsCampaignsCampaignImagesListParams): Observable<__model.CampaignImageRead[]> {
    const pathParams = {
      campaign_id: params.campaign_id,
    };
    return this.http.get<__model.CampaignImageRead[]>(`/api/v1/carefie_campaigns/campaigns/${pathParams.campaign_id}/campaign-images/`);
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_campaign-images_create */
  carefieCampaignsCampaignsCampaignImagesCreate(params: CarefieCampaignsCampaignsCampaignImagesCreateParams): Observable<__model.CampaignImageCreate> {
    const bodyParams = params.data;

    const pathParams = {
      campaign_id: params.campaign_id,
    };
    return this.http.post<__model.CampaignImageCreate>(`/api/v1/carefie_campaigns/campaigns/${pathParams.campaign_id}/campaign-images/`, bodyParams || {});
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_campaign-images_read */
  carefieCampaignsCampaignsCampaignImagesRead(params: CarefieCampaignsCampaignsCampaignImagesReadParams): Observable<__model.CampaignImageRead> {
    const pathParams = {
      campaign_id: params.campaign_id,
      id: params.id,
    };
    return this.http.get<__model.CampaignImageRead>(`/api/v1/carefie_campaigns/campaigns/${pathParams.campaign_id}/campaign-images/${pathParams.id}/`);
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_campaign-images_update */
  carefieCampaignsCampaignsCampaignImagesUpdate(params: CarefieCampaignsCampaignsCampaignImagesUpdateParams): Observable<__model.CampaignImageCreate> {
    const bodyParams = params.data;

    const pathParams = {
      campaign_id: params.campaign_id,
      id: params.id,
    };
    return this.http.put<__model.CampaignImageCreate>(`/api/v1/carefie_campaigns/campaigns/${pathParams.campaign_id}/campaign-images/${pathParams.id}/`, bodyParams || {});
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_campaign-images_partial_update */
  carefieCampaignsCampaignsCampaignImagesPartialUpdate(params: CarefieCampaignsCampaignsCampaignImagesPartialUpdateParams): Observable<__model.CampaignImageCreate> {
    const bodyParams = params.data;

    const pathParams = {
      campaign_id: params.campaign_id,
      id: params.id,
    };
    return this.http.patch<__model.CampaignImageCreate>(`/api/v1/carefie_campaigns/campaigns/${pathParams.campaign_id}/campaign-images/${pathParams.id}/`, bodyParams || {});
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_campaign-images_delete */
  carefieCampaignsCampaignsCampaignImagesDelete(params: CarefieCampaignsCampaignsCampaignImagesDeleteParams): Observable<void> {
    const pathParams = {
      campaign_id: params.campaign_id,
      id: params.id,
    };
    return this.http.delete<void>(`/api/v1/carefie_campaigns/campaigns/${pathParams.campaign_id}/campaign-images/${pathParams.id}/`);
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_organisation-documents_list */
  carefieCampaignsCampaignsOrganisationDocumentsList(params: CarefieCampaignsCampaignsOrganisationDocumentsListParams): Observable<__model.OrganisationDocumentRead[]> {
    const pathParams = {
      campaign_id: params.campaign_id,
    };
    return this.http.get<__model.OrganisationDocumentRead[]>(`/api/v1/carefie_campaigns/campaigns/${pathParams.campaign_id}/organisation-documents/`);
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_organisation-documents_create */
  carefieCampaignsCampaignsOrganisationDocumentsCreate(params: CarefieCampaignsCampaignsOrganisationDocumentsCreateParams): Observable<__model.OrganisationDocumentCreate> {
    const bodyParams = params.data;

    const pathParams = {
      campaign_id: params.campaign_id,
    };
    return this.http.post<__model.OrganisationDocumentCreate>(`/api/v1/carefie_campaigns/campaigns/${pathParams.campaign_id}/organisation-documents/`, bodyParams || {});
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_organisation-documents_read */
  carefieCampaignsCampaignsOrganisationDocumentsRead(params: CarefieCampaignsCampaignsOrganisationDocumentsReadParams): Observable<__model.OrganisationDocumentRead> {
    const pathParams = {
      campaign_id: params.campaign_id,
      id: params.id,
    };
    return this.http.get<__model.OrganisationDocumentRead>(`/api/v1/carefie_campaigns/campaigns/${pathParams.campaign_id}/organisation-documents/${pathParams.id}/`);
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_organisation-documents_update */
  carefieCampaignsCampaignsOrganisationDocumentsUpdate(params: CarefieCampaignsCampaignsOrganisationDocumentsUpdateParams): Observable<__model.OrganisationDocumentCreate> {
    const bodyParams = params.data;

    const pathParams = {
      campaign_id: params.campaign_id,
      id: params.id,
    };
    return this.http.put<__model.OrganisationDocumentCreate>(`/api/v1/carefie_campaigns/campaigns/${pathParams.campaign_id}/organisation-documents/${pathParams.id}/`, bodyParams || {});
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_organisation-documents_partial_update */
  carefieCampaignsCampaignsOrganisationDocumentsPartialUpdate(params: CarefieCampaignsCampaignsOrganisationDocumentsPartialUpdateParams): Observable<__model.OrganisationDocumentCreate> {
    const bodyParams = params.data;

    const pathParams = {
      campaign_id: params.campaign_id,
      id: params.id,
    };
    return this.http.patch<__model.OrganisationDocumentCreate>(`/api/v1/carefie_campaigns/campaigns/${pathParams.campaign_id}/organisation-documents/${pathParams.id}/`, bodyParams || {});
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_organisation-documents_delete */
  carefieCampaignsCampaignsOrganisationDocumentsDelete(params: CarefieCampaignsCampaignsOrganisationDocumentsDeleteParams): Observable<void> {
    const pathParams = {
      campaign_id: params.campaign_id,
      id: params.id,
    };
    return this.http.delete<void>(`/api/v1/carefie_campaigns/campaigns/${pathParams.campaign_id}/organisation-documents/${pathParams.id}/`);
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_read */
  carefieCampaignsCampaignsRead(params: CarefieCampaignsCampaignsReadParams): Observable<__model.CampaignCreation> {
    const pathParams = {
      id: params.id,
    };
    return this.http.get<__model.CampaignCreation>(`/api/v1/carefie_campaigns/campaigns/${pathParams.id}/`);
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_update */
  carefieCampaignsCampaignsUpdate(params: CarefieCampaignsCampaignsUpdateParams): Observable<__model.CampaignCreation> {
    const bodyParams = params.data;

    const pathParams = {
      id: params.id,
    };
    return this.http.put<__model.CampaignCreation>(`/api/v1/carefie_campaigns/campaigns/${pathParams.id}/`, bodyParams || {});
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_partial_update */
  carefieCampaignsCampaignsPartialUpdate(params: CarefieCampaignsCampaignsPartialUpdateParams): Observable<__model.CampaignCreation> {
    const bodyParams = params.data;

    const pathParams = {
      id: params.id,
    };
    return this.http.patch<__model.CampaignCreation>(`/api/v1/carefie_campaigns/campaigns/${pathParams.id}/`, bodyParams || {});
  }

  /** http://develop.carefie.com/swagger/swagger-ui.html#!/carefie_campaigns/carefie_campaigns_campaigns_delete */
  carefieCampaignsCampaignsDelete(params: CarefieCampaignsCampaignsDeleteParams): Observable<void> {
    const pathParams = {
      id: params.id,
    };
    return this.http.delete<void>(`/api/v1/carefie_campaigns/campaigns/${pathParams.id}/`);
  }
}
