/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {Injectable} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CarefieAccountsService} from '../../../controllers/CarefieAccounts';

@Injectable()
export class CarefieAccountsMePartialUpdateFormService {
  form: FormGroup;
  constructor(
    private carefieAccountsService: CarefieAccountsService,
  ) {
    this.form = new FormGroup({
      data: new FormGroup({
        id: new FormControl(undefined, []),
        username: new FormControl(undefined, [Validators.maxLength(150), Validators.minLength(1), Validators.pattern(/^[\w.@+-]+$/), Validators.required]),
        password: new FormControl(undefined, [Validators.maxLength(128), Validators.minLength(1), Validators.required]),
        first_name: new FormControl(undefined, [Validators.minLength(1)]),
        last_name: new FormControl(undefined, []),
        avatar: new FormControl(undefined, []),
        dob: new FormControl(undefined, []),
        gender: new FormControl(undefined, [Validators.minLength(1)]),
        address: new FormControl(undefined, [Validators.minLength(1)]),
        city: new FormControl(undefined, [Validators.minLength(1)]),
        state: new FormControl(undefined, [Validators.minLength(1)]),
        country: new FormControl(undefined, [Validators.minLength(1)]),
        postal_code: new FormControl(undefined, [Validators.minLength(1)]),
        is_carefie_newsletter_subscribed: new FormControl(undefined, []),
        is_org_allowed_to_contact: new FormControl(undefined, []),
        is_donors_allowed_to_send_friends_request: new FormControl(undefined, []),
        is_allowed_to_display_carefies_on_profile_page: new FormControl(undefined, []),
        is_campaign_updates_subscribed: new FormControl(undefined, []),
        is_receive_thank_you_message: new FormControl(undefined, []),
      }, [Validators.required]),
    });
  }

  submit(raw = false) {
    const data = raw ?
      this.form.getRawValue() :
      this.form.value;
    return this.carefieAccountsService.carefieAccountsMePartialUpdate(data);
  }
}
