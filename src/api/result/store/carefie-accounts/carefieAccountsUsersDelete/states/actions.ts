/* tslint:disable:max-line-length max-classes-per-file */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {HttpErrorResponse} from '@angular/common/http';
import {Action} from '@ngrx/store';
import {CarefieAccountsUsersDeleteParams} from '../../../../controllers/CarefieAccounts';

export enum Actions {
  START = '[CarefieAccounts carefieAccountsUsersDelete] Start',
  SUCCESS = '[CarefieAccounts carefieAccountsUsersDelete] Success',
  ERROR = '[CarefieAccounts carefieAccountsUsersDelete] Error',
}

export class Start implements Action {
  readonly type = Actions.START;
  constructor(public payload: CarefieAccountsUsersDeleteParams) {}
}

export class Success implements Action {
  readonly type = Actions.SUCCESS;
  constructor(public payload: void) {}
}

export class Error implements Action {
  readonly type = Actions.ERROR;
  constructor(public payload: HttpErrorResponse) {}
}

export type CarefieAccountsUsersDeleteAction = Start | Success | Error;
