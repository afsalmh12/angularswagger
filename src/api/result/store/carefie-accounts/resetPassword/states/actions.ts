/* tslint:disable:max-line-length max-classes-per-file */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {HttpErrorResponse} from '@angular/common/http';
import {Action} from '@ngrx/store';
import {ResetPasswordParams} from '../../../../controllers/CarefieAccounts';
import * as __model from '../../../../model';

export enum Actions {
  START = '[CarefieAccounts resetPassword] Start',
  SUCCESS = '[CarefieAccounts resetPassword] Success',
  ERROR = '[CarefieAccounts resetPassword] Error',
}

export class Start implements Action {
  readonly type = Actions.START;
  constructor(public payload: ResetPasswordParams) {}
}

export class Success implements Action {
  readonly type = Actions.SUCCESS;
  constructor(public payload: __model.ResetPassword) {}
}

export class Error implements Action {
  readonly type = Actions.ERROR;
  constructor(public payload: HttpErrorResponse) {}
}

export type ResetPasswordAction = Start | Success | Error;
