/* tslint:disable:max-line-length max-classes-per-file */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {HttpErrorResponse} from '@angular/common/http';
import {Action} from '@ngrx/store';
import {CarefieAccountsMeUpdateParams} from '../../../../controllers/CarefieAccounts';
import * as __model from '../../../../model';

export enum Actions {
  START = '[CarefieAccounts carefieAccountsMeUpdate] Start',
  SUCCESS = '[CarefieAccounts carefieAccountsMeUpdate] Success',
  ERROR = '[CarefieAccounts carefieAccountsMeUpdate] Error',
}

export class Start implements Action {
  readonly type = Actions.START;
  constructor(public payload: CarefieAccountsMeUpdateParams) {}
}

export class Success implements Action {
  readonly type = Actions.SUCCESS;
  constructor(public payload: __model.User) {}
}

export class Error implements Action {
  readonly type = Actions.ERROR;
  constructor(public payload: HttpErrorResponse) {}
}

export type CarefieAccountsMeUpdateAction = Start | Success | Error;
