/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {createFeatureSelector} from '@ngrx/store';

import {HttpErrorResponse} from '@angular/common/http';
import * as __model from '../../../../model';
import * as actions from './actions';

export interface CarefieAccountsMeUpdateState {
  data: __model.User | null;
  loading: boolean;
  error: HttpErrorResponse | null;
}

export const initialCarefieAccountsMeUpdateState: CarefieAccountsMeUpdateState = {
  data: null,
  loading: false,
  error: null,
};

export const selectorName = 'CarefieAccounts_CarefieAccountsMeUpdate';
export const getCarefieAccountsMeUpdateStateSelector = createFeatureSelector<CarefieAccountsMeUpdateState>(selectorName);

export function CarefieAccountsMeUpdateReducer(
  state: CarefieAccountsMeUpdateState = initialCarefieAccountsMeUpdateState,
  action: actions.CarefieAccountsMeUpdateAction): CarefieAccountsMeUpdateState {
  switch (action.type) {
    case actions.Actions.START: return {...state, loading: true, error: null};
    case actions.Actions.SUCCESS: return {...state, data: action.payload, loading: false};
    case actions.Actions.ERROR: return {...state, error: action.payload, loading: false};
    default: return state;
  }
}
