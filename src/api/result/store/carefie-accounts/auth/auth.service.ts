/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {Injectable} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CarefieAccountsService} from '../../../controllers/CarefieAccounts';

@Injectable()
export class AuthFormService {
  form: FormGroup;
  constructor(
    private carefieAccountsService: CarefieAccountsService,
  ) {
    this.form = new FormGroup({
      data: new FormGroup({
        username: new FormControl(undefined, [Validators.minLength(1), Validators.required]),
        password: new FormControl(undefined, [Validators.minLength(1), Validators.required]),
      }, [Validators.required]),
    });
  }

  submit(raw = false) {
    const data = raw ?
      this.form.getRawValue() :
      this.form.value;
    return this.carefieAccountsService.auth(data);
  }
}
