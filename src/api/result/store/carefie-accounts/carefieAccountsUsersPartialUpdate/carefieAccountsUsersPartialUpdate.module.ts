/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {NgModule} from '@angular/core';
import {EffectsModule as NgrxEffectsModule} from '@ngrx/effects';
import {StoreModule as NgrxStoreModule} from '@ngrx/store';

import {CarefieAccountsService} from '../../../controllers/CarefieAccounts';
import {FormsSharedModule} from '../../forms-shared.module';
import {CarefieAccountsUsersPartialUpdateFormService} from './carefieAccountsUsersPartialUpdate.service';

import {CarefieAccountsUsersPartialUpdateEffects} from './states/effects';
import {CarefieAccountsUsersPartialUpdateReducer} from './states/reducers';
import {selectorName} from './states/reducers';

@NgModule({
  imports: [
    FormsSharedModule,
    NgrxStoreModule.forFeature(selectorName, CarefieAccountsUsersPartialUpdateReducer),
    NgrxEffectsModule.forFeature([CarefieAccountsUsersPartialUpdateEffects]),
  ],
  providers: [
    CarefieAccountsService,
    CarefieAccountsUsersPartialUpdateFormService,
  ],
})
export class CarefieAccountsUsersPartialUpdateModule {}
