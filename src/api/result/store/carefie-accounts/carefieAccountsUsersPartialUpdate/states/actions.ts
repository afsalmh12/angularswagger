/* tslint:disable:max-line-length max-classes-per-file */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {HttpErrorResponse} from '@angular/common/http';
import {Action} from '@ngrx/store';
import {CarefieAccountsUsersPartialUpdateParams} from '../../../../controllers/CarefieAccounts';
import * as __model from '../../../../model';

export enum Actions {
  START = '[CarefieAccounts carefieAccountsUsersPartialUpdate] Start',
  SUCCESS = '[CarefieAccounts carefieAccountsUsersPartialUpdate] Success',
  ERROR = '[CarefieAccounts carefieAccountsUsersPartialUpdate] Error',
}

export class Start implements Action {
  readonly type = Actions.START;
  constructor(public payload: CarefieAccountsUsersPartialUpdateParams) {}
}

export class Success implements Action {
  readonly type = Actions.SUCCESS;
  constructor(public payload: __model.User) {}
}

export class Error implements Action {
  readonly type = Actions.ERROR;
  constructor(public payload: HttpErrorResponse) {}
}

export type CarefieAccountsUsersPartialUpdateAction = Start | Success | Error;
