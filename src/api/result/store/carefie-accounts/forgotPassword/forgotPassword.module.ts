/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {NgModule} from '@angular/core';
import {EffectsModule as NgrxEffectsModule} from '@ngrx/effects';
import {StoreModule as NgrxStoreModule} from '@ngrx/store';

import {CarefieAccountsService} from '../../../controllers/CarefieAccounts';
import {FormsSharedModule} from '../../forms-shared.module';
import {ForgotPasswordFormService} from './forgotPassword.service';

import {ForgotPasswordEffects} from './states/effects';
import {ForgotPasswordReducer} from './states/reducers';
import {selectorName} from './states/reducers';

@NgModule({
  imports: [
    FormsSharedModule,
    NgrxStoreModule.forFeature(selectorName, ForgotPasswordReducer),
    NgrxEffectsModule.forFeature([ForgotPasswordEffects]),
  ],
  providers: [
    CarefieAccountsService,
    ForgotPasswordFormService,
  ],
})
export class ForgotPasswordModule {}
