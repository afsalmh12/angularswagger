/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {createFeatureSelector} from '@ngrx/store';

import {HttpErrorResponse} from '@angular/common/http';
import * as __model from '../../../../model';
import * as actions from './actions';

export interface CarefieAccountsUsersUpdateState {
  data: __model.User | null;
  loading: boolean;
  error: HttpErrorResponse | null;
}

export const initialCarefieAccountsUsersUpdateState: CarefieAccountsUsersUpdateState = {
  data: null,
  loading: false,
  error: null,
};

export const selectorName = 'CarefieAccounts_CarefieAccountsUsersUpdate';
export const getCarefieAccountsUsersUpdateStateSelector = createFeatureSelector<CarefieAccountsUsersUpdateState>(selectorName);

export function CarefieAccountsUsersUpdateReducer(
  state: CarefieAccountsUsersUpdateState = initialCarefieAccountsUsersUpdateState,
  action: actions.CarefieAccountsUsersUpdateAction): CarefieAccountsUsersUpdateState {
  switch (action.type) {
    case actions.Actions.START: return {...state, loading: true, error: null};
    case actions.Actions.SUCCESS: return {...state, data: action.payload, loading: false};
    case actions.Actions.ERROR: return {...state, error: action.payload, loading: false};
    default: return state;
  }
}
