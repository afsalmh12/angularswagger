/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {NgModule} from '@angular/core';
import {EffectsModule as NgrxEffectsModule} from '@ngrx/effects';
import {StoreModule as NgrxStoreModule} from '@ngrx/store';

import {CarefieAccountsService} from '../../../controllers/CarefieAccounts';
import {FormsSharedModule} from '../../forms-shared.module';
import {CarefieAccountsUsersReadFormService} from './carefieAccountsUsersRead.service';

import {CarefieAccountsUsersReadEffects} from './states/effects';
import {CarefieAccountsUsersReadReducer} from './states/reducers';
import {selectorName} from './states/reducers';

@NgModule({
  imports: [
    FormsSharedModule,
    NgrxStoreModule.forFeature(selectorName, CarefieAccountsUsersReadReducer),
    NgrxEffectsModule.forFeature([CarefieAccountsUsersReadEffects]),
  ],
  providers: [
    CarefieAccountsService,
    CarefieAccountsUsersReadFormService,
  ],
})
export class CarefieAccountsUsersReadModule {}
