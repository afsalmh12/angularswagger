/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {Injectable} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CarefieAccountsService} from '../../../controllers/CarefieAccounts';

@Injectable()
export class CarefieAccountsUsersReadFormService {
  form: FormGroup;
  constructor(
    private carefieAccountsService: CarefieAccountsService,
  ) {
    this.form = new FormGroup({
      id: new FormControl(undefined, [Validators.required]),
    });
  }

  submit(raw = false) {
    const data = raw ?
      this.form.getRawValue() :
      this.form.value;
    return this.carefieAccountsService.carefieAccountsUsersRead(data);
  }
}
