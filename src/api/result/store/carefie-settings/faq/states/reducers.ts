/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {createFeatureSelector} from '@ngrx/store';

import {HttpErrorResponse} from '@angular/common/http';
import * as __model from '../../../../model';
import * as actions from './actions';

export interface FaqState {
  data: __model.Category[] | null;
  loading: boolean;
  error: HttpErrorResponse | null;
}

export const initialFaqState: FaqState = {
  data: null,
  loading: false,
  error: null,
};

export const selectorName = 'CarefieSettings_Faq';
export const getFaqStateSelector = createFeatureSelector<FaqState>(selectorName);

export function FaqReducer(
  state: FaqState = initialFaqState,
  action: actions.FaqAction): FaqState {
  switch (action.type) {
    case actions.Actions.START: return {...state, loading: true, error: null};
    case actions.Actions.SUCCESS: return {...state, data: action.payload, loading: false};
    case actions.Actions.ERROR: return {...state, error: action.payload, loading: false};
    default: return state;
  }
}
