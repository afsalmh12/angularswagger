/* tslint:disable:max-line-length max-classes-per-file */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {HttpErrorResponse} from '@angular/common/http';
import {Action} from '@ngrx/store';
import {ContactParams} from '../../../../controllers/CarefieSettings';
import * as __model from '../../../../model';

export enum Actions {
  START = '[CarefieSettings contact] Start',
  SUCCESS = '[CarefieSettings contact] Success',
  ERROR = '[CarefieSettings contact] Error',
}

export class Start implements Action {
  readonly type = Actions.START;
  constructor(public payload: ContactParams) {}
}

export class Success implements Action {
  readonly type = Actions.SUCCESS;
  constructor(public payload: __model.ContactUs) {}
}

export class Error implements Action {
  readonly type = Actions.ERROR;
  constructor(public payload: HttpErrorResponse) {}
}

export type ContactAction = Start | Success | Error;
