/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {Injectable} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CarefieSettingsService} from '../../../controllers/CarefieSettings';

@Injectable()
export class ContactFormService {
  form: FormGroup;
  constructor(
    private carefieSettingsService: CarefieSettingsService,
  ) {
    this.form = new FormGroup({
      data: new FormGroup({
        name: new FormControl(undefined, [Validators.minLength(1), Validators.required]),
        email: new FormControl(undefined, [Validators.email, Validators.minLength(1), Validators.required]),
        subject: new FormControl(undefined, [Validators.minLength(1), Validators.required]),
        message: new FormControl(undefined, [Validators.minLength(1), Validators.required]),
      }, [Validators.required]),
    });
  }

  submit(raw = false) {
    const data = raw ?
      this.form.getRawValue() :
      this.form.value;
    return this.carefieSettingsService.contact(data);
  }
}
