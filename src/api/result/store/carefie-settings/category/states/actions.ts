/* tslint:disable:max-line-length max-classes-per-file */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {HttpErrorResponse} from '@angular/common/http';
import {Action} from '@ngrx/store';
import * as __model from '../../../../model';

export enum Actions {
  START = '[CarefieSettings category] Start',
  SUCCESS = '[CarefieSettings category] Success',
  ERROR = '[CarefieSettings category] Error',
}

export class Start implements Action {
  readonly type = Actions.START;
  constructor() {}
}

export class Success implements Action {
  readonly type = Actions.SUCCESS;
  constructor(public payload: __model.CampaignCategory[]) {}
}

export class Error implements Action {
  readonly type = Actions.ERROR;
  constructor(public payload: HttpErrorResponse) {}
}

export type CategoryAction = Start | Success | Error;
