/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {NgModule} from '@angular/core';
import {EffectsModule as NgrxEffectsModule} from '@ngrx/effects';
import {StoreModule as NgrxStoreModule} from '@ngrx/store';

import {CarefieSettingsService} from '../../../controllers/CarefieSettings';
import {FormsSharedModule} from '../../forms-shared.module';

import {CategoryEffects} from './states/effects';
import {CategoryReducer} from './states/reducers';
import {selectorName} from './states/reducers';

@NgModule({
  imports: [
    FormsSharedModule,
    NgrxStoreModule.forFeature(selectorName, CategoryReducer),
    NgrxEffectsModule.forFeature([CategoryEffects]),
  ],
  providers: [
    CarefieSettingsService,
  ],
})
export class CategoryModule {}
