/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {NgModule} from '@angular/core';
import {EffectsModule as NgrxEffectsModule} from '@ngrx/effects';
import {StoreModule as NgrxStoreModule} from '@ngrx/store';

import {CarefieCampaignsService} from '../../../controllers/CarefieCampaigns';
import {FormsSharedModule} from '../../forms-shared.module';
import {CarefieCampaignsCampaignsDeleteFormService} from './carefieCampaignsCampaignsDelete.service';

import {CarefieCampaignsCampaignsDeleteEffects} from './states/effects';
import {CarefieCampaignsCampaignsDeleteReducer} from './states/reducers';
import {selectorName} from './states/reducers';

@NgModule({
  imports: [
    FormsSharedModule,
    NgrxStoreModule.forFeature(selectorName, CarefieCampaignsCampaignsDeleteReducer),
    NgrxEffectsModule.forFeature([CarefieCampaignsCampaignsDeleteEffects]),
  ],
  providers: [
    CarefieCampaignsService,
    CarefieCampaignsCampaignsDeleteFormService,
  ],
})
export class CarefieCampaignsCampaignsDeleteModule {}
