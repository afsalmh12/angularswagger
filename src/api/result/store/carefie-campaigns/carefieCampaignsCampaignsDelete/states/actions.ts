/* tslint:disable:max-line-length max-classes-per-file */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {HttpErrorResponse} from '@angular/common/http';
import {Action} from '@ngrx/store';
import {CarefieCampaignsCampaignsDeleteParams} from '../../../../controllers/CarefieCampaigns';

export enum Actions {
  START = '[CarefieCampaigns carefieCampaignsCampaignsDelete] Start',
  SUCCESS = '[CarefieCampaigns carefieCampaignsCampaignsDelete] Success',
  ERROR = '[CarefieCampaigns carefieCampaignsCampaignsDelete] Error',
}

export class Start implements Action {
  readonly type = Actions.START;
  constructor(public payload: CarefieCampaignsCampaignsDeleteParams) {}
}

export class Success implements Action {
  readonly type = Actions.SUCCESS;
  constructor(public payload: void) {}
}

export class Error implements Action {
  readonly type = Actions.ERROR;
  constructor(public payload: HttpErrorResponse) {}
}

export type CarefieCampaignsCampaignsDeleteAction = Start | Success | Error;
