/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {createFeatureSelector} from '@ngrx/store';

import {HttpErrorResponse} from '@angular/common/http';
import * as __model from '../../../../model';
import * as actions from './actions';

export interface CarefieCampaignsCampaignsListState {
  data: __model.CampaignCreation[] | null;
  loading: boolean;
  error: HttpErrorResponse | null;
}

export const initialCarefieCampaignsCampaignsListState: CarefieCampaignsCampaignsListState = {
  data: null,
  loading: false,
  error: null,
};

export const selectorName = 'CarefieCampaigns_CarefieCampaignsCampaignsList';
export const getCarefieCampaignsCampaignsListStateSelector = createFeatureSelector<CarefieCampaignsCampaignsListState>(selectorName);

export function CarefieCampaignsCampaignsListReducer(
  state: CarefieCampaignsCampaignsListState = initialCarefieCampaignsCampaignsListState,
  action: actions.CarefieCampaignsCampaignsListAction): CarefieCampaignsCampaignsListState {
  switch (action.type) {
    case actions.Actions.START: return {...state, loading: true, error: null};
    case actions.Actions.SUCCESS: return {...state, data: action.payload, loading: false};
    case actions.Actions.ERROR: return {...state, error: action.payload, loading: false};
    default: return state;
  }
}
