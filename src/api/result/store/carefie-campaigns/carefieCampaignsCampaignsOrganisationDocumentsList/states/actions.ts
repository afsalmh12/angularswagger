/* tslint:disable:max-line-length max-classes-per-file */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {HttpErrorResponse} from '@angular/common/http';
import {Action} from '@ngrx/store';
import {CarefieCampaignsCampaignsOrganisationDocumentsListParams} from '../../../../controllers/CarefieCampaigns';
import * as __model from '../../../../model';

export enum Actions {
  START = '[CarefieCampaigns carefieCampaignsCampaignsOrganisationDocumentsList] Start',
  SUCCESS = '[CarefieCampaigns carefieCampaignsCampaignsOrganisationDocumentsList] Success',
  ERROR = '[CarefieCampaigns carefieCampaignsCampaignsOrganisationDocumentsList] Error',
}

export class Start implements Action {
  readonly type = Actions.START;
  constructor(public payload: CarefieCampaignsCampaignsOrganisationDocumentsListParams) {}
}

export class Success implements Action {
  readonly type = Actions.SUCCESS;
  constructor(public payload: __model.OrganisationDocumentRead[]) {}
}

export class Error implements Action {
  readonly type = Actions.ERROR;
  constructor(public payload: HttpErrorResponse) {}
}

export type CarefieCampaignsCampaignsOrganisationDocumentsListAction = Start | Success | Error;
