/* tslint:disable:max-line-length max-classes-per-file */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {HttpErrorResponse} from '@angular/common/http';
import {Action} from '@ngrx/store';
import {CarefieCampaignsCampaignsBankDocumentsPartialUpdateParams} from '../../../../controllers/CarefieCampaigns';
import * as __model from '../../../../model';

export enum Actions {
  START = '[CarefieCampaigns carefieCampaignsCampaignsBankDocumentsPartialUpdate] Start',
  SUCCESS = '[CarefieCampaigns carefieCampaignsCampaignsBankDocumentsPartialUpdate] Success',
  ERROR = '[CarefieCampaigns carefieCampaignsCampaignsBankDocumentsPartialUpdate] Error',
}

export class Start implements Action {
  readonly type = Actions.START;
  constructor(public payload: CarefieCampaignsCampaignsBankDocumentsPartialUpdateParams) {}
}

export class Success implements Action {
  readonly type = Actions.SUCCESS;
  constructor(public payload: __model.BankDocumentCreate) {}
}

export class Error implements Action {
  readonly type = Actions.ERROR;
  constructor(public payload: HttpErrorResponse) {}
}

export type CarefieCampaignsCampaignsBankDocumentsPartialUpdateAction = Start | Success | Error;
