/* tslint:disable:max-line-length max-classes-per-file */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {HttpErrorResponse} from '@angular/common/http';
import {Action} from '@ngrx/store';
import {CarefieCampaignsCampaignsCampaignImagesDeleteParams} from '../../../../controllers/CarefieCampaigns';

export enum Actions {
  START = '[CarefieCampaigns carefieCampaignsCampaignsCampaignImagesDelete] Start',
  SUCCESS = '[CarefieCampaigns carefieCampaignsCampaignsCampaignImagesDelete] Success',
  ERROR = '[CarefieCampaigns carefieCampaignsCampaignsCampaignImagesDelete] Error',
}

export class Start implements Action {
  readonly type = Actions.START;
  constructor(public payload: CarefieCampaignsCampaignsCampaignImagesDeleteParams) {}
}

export class Success implements Action {
  readonly type = Actions.SUCCESS;
  constructor(public payload: void) {}
}

export class Error implements Action {
  readonly type = Actions.ERROR;
  constructor(public payload: HttpErrorResponse) {}
}

export type CarefieCampaignsCampaignsCampaignImagesDeleteAction = Start | Success | Error;
