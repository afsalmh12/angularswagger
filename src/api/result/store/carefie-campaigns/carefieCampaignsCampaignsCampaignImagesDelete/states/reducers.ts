/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {createFeatureSelector} from '@ngrx/store';

import {HttpErrorResponse} from '@angular/common/http';
import * as actions from './actions';

export interface CarefieCampaignsCampaignsCampaignImagesDeleteState {
  data: void | null;
  loading: boolean;
  error: HttpErrorResponse | null;
}

export const initialCarefieCampaignsCampaignsCampaignImagesDeleteState: CarefieCampaignsCampaignsCampaignImagesDeleteState = {
  data: null,
  loading: false,
  error: null,
};

export const selectorName = 'CarefieCampaigns_CarefieCampaignsCampaignsCampaignImagesDelete';
export const getCarefieCampaignsCampaignsCampaignImagesDeleteStateSelector = createFeatureSelector<CarefieCampaignsCampaignsCampaignImagesDeleteState>(selectorName);

export function CarefieCampaignsCampaignsCampaignImagesDeleteReducer(
  state: CarefieCampaignsCampaignsCampaignImagesDeleteState = initialCarefieCampaignsCampaignsCampaignImagesDeleteState,
  action: actions.CarefieCampaignsCampaignsCampaignImagesDeleteAction): CarefieCampaignsCampaignsCampaignImagesDeleteState {
  switch (action.type) {
    case actions.Actions.START: return {...state, loading: true, error: null};
    case actions.Actions.SUCCESS: return {...state, data: action.payload, loading: false};
    case actions.Actions.ERROR: return {...state, error: action.payload, loading: false};
    default: return state;
  }
}
