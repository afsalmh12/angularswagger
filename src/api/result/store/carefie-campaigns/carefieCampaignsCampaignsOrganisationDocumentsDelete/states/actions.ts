/* tslint:disable:max-line-length max-classes-per-file */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {HttpErrorResponse} from '@angular/common/http';
import {Action} from '@ngrx/store';
import {CarefieCampaignsCampaignsOrganisationDocumentsDeleteParams} from '../../../../controllers/CarefieCampaigns';

export enum Actions {
  START = '[CarefieCampaigns carefieCampaignsCampaignsOrganisationDocumentsDelete] Start',
  SUCCESS = '[CarefieCampaigns carefieCampaignsCampaignsOrganisationDocumentsDelete] Success',
  ERROR = '[CarefieCampaigns carefieCampaignsCampaignsOrganisationDocumentsDelete] Error',
}

export class Start implements Action {
  readonly type = Actions.START;
  constructor(public payload: CarefieCampaignsCampaignsOrganisationDocumentsDeleteParams) {}
}

export class Success implements Action {
  readonly type = Actions.SUCCESS;
  constructor(public payload: void) {}
}

export class Error implements Action {
  readonly type = Actions.ERROR;
  constructor(public payload: HttpErrorResponse) {}
}

export type CarefieCampaignsCampaignsOrganisationDocumentsDeleteAction = Start | Success | Error;
