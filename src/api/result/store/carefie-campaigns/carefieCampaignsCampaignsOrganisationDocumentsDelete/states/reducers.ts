/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {createFeatureSelector} from '@ngrx/store';

import {HttpErrorResponse} from '@angular/common/http';
import * as actions from './actions';

export interface CarefieCampaignsCampaignsOrganisationDocumentsDeleteState {
  data: void | null;
  loading: boolean;
  error: HttpErrorResponse | null;
}

export const initialCarefieCampaignsCampaignsOrganisationDocumentsDeleteState: CarefieCampaignsCampaignsOrganisationDocumentsDeleteState = {
  data: null,
  loading: false,
  error: null,
};

export const selectorName = 'CarefieCampaigns_CarefieCampaignsCampaignsOrganisationDocumentsDelete';
export const getCarefieCampaignsCampaignsOrganisationDocumentsDeleteStateSelector = createFeatureSelector<CarefieCampaignsCampaignsOrganisationDocumentsDeleteState>(selectorName);

export function CarefieCampaignsCampaignsOrganisationDocumentsDeleteReducer(
  state: CarefieCampaignsCampaignsOrganisationDocumentsDeleteState = initialCarefieCampaignsCampaignsOrganisationDocumentsDeleteState,
  action: actions.CarefieCampaignsCampaignsOrganisationDocumentsDeleteAction): CarefieCampaignsCampaignsOrganisationDocumentsDeleteState {
  switch (action.type) {
    case actions.Actions.START: return {...state, loading: true, error: null};
    case actions.Actions.SUCCESS: return {...state, data: action.payload, loading: false};
    case actions.Actions.ERROR: return {...state, error: action.payload, loading: false};
    default: return state;
  }
}
