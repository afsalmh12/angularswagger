/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {createFeatureSelector} from '@ngrx/store';

import {HttpErrorResponse} from '@angular/common/http';
import * as __model from '../../../../model';
import * as actions from './actions';

export interface CarefieCampaignsCampaignsOrganisationDocumentsPartialUpdateState {
  data: __model.OrganisationDocumentCreate | null;
  loading: boolean;
  error: HttpErrorResponse | null;
}

export const initialCarefieCampaignsCampaignsOrganisationDocumentsPartialUpdateState: CarefieCampaignsCampaignsOrganisationDocumentsPartialUpdateState = {
  data: null,
  loading: false,
  error: null,
};

export const selectorName = 'CarefieCampaigns_CarefieCampaignsCampaignsOrganisationDocumentsPartialUpdate';
export const getCarefieCampaignsCampaignsOrganisationDocumentsPartialUpdateStateSelector = createFeatureSelector<CarefieCampaignsCampaignsOrganisationDocumentsPartialUpdateState>(selectorName);

export function CarefieCampaignsCampaignsOrganisationDocumentsPartialUpdateReducer(
  state: CarefieCampaignsCampaignsOrganisationDocumentsPartialUpdateState = initialCarefieCampaignsCampaignsOrganisationDocumentsPartialUpdateState,
  action: actions.CarefieCampaignsCampaignsOrganisationDocumentsPartialUpdateAction): CarefieCampaignsCampaignsOrganisationDocumentsPartialUpdateState {
  switch (action.type) {
    case actions.Actions.START: return {...state, loading: true, error: null};
    case actions.Actions.SUCCESS: return {...state, data: action.payload, loading: false};
    case actions.Actions.ERROR: return {...state, error: action.payload, loading: false};
    default: return state;
  }
}
