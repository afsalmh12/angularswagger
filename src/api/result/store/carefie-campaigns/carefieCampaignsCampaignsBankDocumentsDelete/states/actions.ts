/* tslint:disable:max-line-length max-classes-per-file */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {HttpErrorResponse} from '@angular/common/http';
import {Action} from '@ngrx/store';
import {CarefieCampaignsCampaignsBankDocumentsDeleteParams} from '../../../../controllers/CarefieCampaigns';

export enum Actions {
  START = '[CarefieCampaigns carefieCampaignsCampaignsBankDocumentsDelete] Start',
  SUCCESS = '[CarefieCampaigns carefieCampaignsCampaignsBankDocumentsDelete] Success',
  ERROR = '[CarefieCampaigns carefieCampaignsCampaignsBankDocumentsDelete] Error',
}

export class Start implements Action {
  readonly type = Actions.START;
  constructor(public payload: CarefieCampaignsCampaignsBankDocumentsDeleteParams) {}
}

export class Success implements Action {
  readonly type = Actions.SUCCESS;
  constructor(public payload: void) {}
}

export class Error implements Action {
  readonly type = Actions.ERROR;
  constructor(public payload: HttpErrorResponse) {}
}

export type CarefieCampaignsCampaignsBankDocumentsDeleteAction = Start | Success | Error;
