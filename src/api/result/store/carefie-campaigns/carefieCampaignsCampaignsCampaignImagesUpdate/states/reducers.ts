/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {createFeatureSelector} from '@ngrx/store';

import {HttpErrorResponse} from '@angular/common/http';
import * as __model from '../../../../model';
import * as actions from './actions';

export interface CarefieCampaignsCampaignsCampaignImagesUpdateState {
  data: __model.CampaignImageCreate | null;
  loading: boolean;
  error: HttpErrorResponse | null;
}

export const initialCarefieCampaignsCampaignsCampaignImagesUpdateState: CarefieCampaignsCampaignsCampaignImagesUpdateState = {
  data: null,
  loading: false,
  error: null,
};

export const selectorName = 'CarefieCampaigns_CarefieCampaignsCampaignsCampaignImagesUpdate';
export const getCarefieCampaignsCampaignsCampaignImagesUpdateStateSelector = createFeatureSelector<CarefieCampaignsCampaignsCampaignImagesUpdateState>(selectorName);

export function CarefieCampaignsCampaignsCampaignImagesUpdateReducer(
  state: CarefieCampaignsCampaignsCampaignImagesUpdateState = initialCarefieCampaignsCampaignsCampaignImagesUpdateState,
  action: actions.CarefieCampaignsCampaignsCampaignImagesUpdateAction): CarefieCampaignsCampaignsCampaignImagesUpdateState {
  switch (action.type) {
    case actions.Actions.START: return {...state, loading: true, error: null};
    case actions.Actions.SUCCESS: return {...state, data: action.payload, loading: false};
    case actions.Actions.ERROR: return {...state, error: action.payload, loading: false};
    default: return state;
  }
}
