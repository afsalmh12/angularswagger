/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {Injectable} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CarefieCampaignsService} from '../../../controllers/CarefieCampaigns';

@Injectable()
export class CarefieCampaignsCampaignsPartialUpdateFormService {
  form: FormGroup;
  constructor(
    private carefieCampaignsService: CarefieCampaignsService,
  ) {
    this.form = new FormGroup({
      data: new FormGroup({
        id: new FormControl(undefined, []),
        title: new FormControl(undefined, [Validators.maxLength(250)]),
        category: new FormControl(undefined, [Validators.required]),
        goal: new FormControl(undefined, []),
        goal_type: new FormControl(undefined, [Validators.maxLength(250)]),
        duration_days: new FormControl(undefined, [Validators.max(2147483647), Validators.min(-2147483648)]),
        org_name: new FormControl(undefined, [Validators.maxLength(250)]),
        org_street_address: new FormControl(undefined, [Validators.maxLength(350)]),
        org_city: new FormControl(undefined, [Validators.maxLength(250)]),
        org_state: new FormControl(undefined, [Validators.maxLength(250)]),
        org_country: new FormControl(undefined, [Validators.maxLength(250)]),
        org_postal_code: new FormControl(undefined, [Validators.maxLength(250)]),
        org_country_code: new FormControl(undefined, [Validators.maxLength(250)]),
        org_telephone_no: new FormControl(undefined, [Validators.maxLength(250)]),
        org_website: new FormControl(undefined, [Validators.maxLength(250)]),
        org_email_address: new FormControl(undefined, [Validators.maxLength(250)]),
        org_contact_person_title: new FormControl(undefined, [Validators.maxLength(250)]),
        org_contact_person_first_name: new FormControl(undefined, [Validators.maxLength(250)]),
        org_contact_person_last_name: new FormControl(undefined, [Validators.maxLength(250)]),
        org_registration_date: new FormControl(undefined, []),
        org_registration_no: new FormControl(undefined, [Validators.maxLength(250)]),
        org_EIN: new FormControl(undefined, [Validators.maxLength(250)]),
        org_mission_statement: new FormControl(undefined, []),
        org_principal_officer_title: new FormControl(undefined, [Validators.maxLength(250)]),
        org_principal_officer_first_name: new FormControl(undefined, [Validators.maxLength(250)]),
        org_principal_officer_last_name: new FormControl(undefined, [Validators.maxLength(250)]),
        is_shipping_address_same_org_address: new FormControl(undefined, []),
        shipping_street_address: new FormControl(undefined, [Validators.maxLength(350)]),
        shipping_city: new FormControl(undefined, [Validators.maxLength(250)]),
        shipping_state: new FormControl(undefined, [Validators.maxLength(250)]),
        shipping_country: new FormControl(undefined, [Validators.maxLength(250)]),
        shipping_postal_code: new FormControl(undefined, [Validators.maxLength(250)]),
        shipping_country_code: new FormControl(undefined, [Validators.maxLength(250)]),
        shipping_telephone_no: new FormControl(undefined, [Validators.maxLength(250)]),
        shipping_contact_person_title: new FormControl(undefined, [Validators.maxLength(250)]),
        shipping_contact_person_first_name: new FormControl(undefined, [Validators.maxLength(250)]),
        shipping_contact_person_last_name: new FormControl(undefined, [Validators.maxLength(250)]),
        summary: new FormControl(undefined, []),
        introduction: new FormControl(undefined, []),
        describe_your_problem: new FormControl(undefined, []),
        how_make_things_better: new FormControl(undefined, []),
        video_url: new FormControl(undefined, [Validators.maxLength(250)]),
        banner_image: new FormControl(undefined, []),
        bank_name: new FormControl(undefined, [Validators.maxLength(250)]),
        bank_account_holder_name: new FormControl(undefined, [Validators.maxLength(250)]),
        bank_account_no: new FormControl(undefined, [Validators.maxLength(250)]),
        bank_BIC_or_SWIFT_code: new FormControl(undefined, [Validators.maxLength(250)]),
        bank_national_clearing_code: new FormControl(undefined, [Validators.maxLength(250)]),
        bank_branch_or_sort_code: new FormControl(undefined, [Validators.maxLength(250)]),
        custom_campaign_url: new FormControl(undefined, [Validators.maxLength(250)]),
        is_regular_giving_for_my_campaign_enabled: new FormControl(undefined, []),
        how_donation_will_make_impact: new FormControl(undefined, []),
        is_standard_thanks_msg_used: new FormControl(undefined, []),
        custom_thanks_msg: new FormControl(undefined, []),
      }, [Validators.required]),
      id: new FormControl(undefined, [Validators.required]),
    });
  }

  submit(raw = false) {
    const data = raw ?
      this.form.getRawValue() :
      this.form.value;
    return this.carefieCampaignsService.carefieCampaignsCampaignsPartialUpdate(data);
  }
}
