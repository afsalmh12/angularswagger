/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {Injectable} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CarefieCampaignsService} from '../../../controllers/CarefieCampaigns';

@Injectable()
export class CarefieCampaignsCampaignsReadFormService {
  form: FormGroup;
  constructor(
    private carefieCampaignsService: CarefieCampaignsService,
  ) {
    this.form = new FormGroup({
      id: new FormControl(undefined, [Validators.required]),
    });
  }

  submit(raw = false) {
    const data = raw ?
      this.form.getRawValue() :
      this.form.value;
    return this.carefieCampaignsService.carefieCampaignsCampaignsRead(data);
  }
}
