/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import {createFeatureSelector} from '@ngrx/store';

import {HttpErrorResponse} from '@angular/common/http';
import * as __model from '../../../../model';
import * as actions from './actions';

export interface CarefieCampaignsCampaignsCreateState {
  data: __model.CampaignCreation | null;
  loading: boolean;
  error: HttpErrorResponse | null;
}

export const initialCarefieCampaignsCampaignsCreateState: CarefieCampaignsCampaignsCreateState = {
  data: null,
  loading: false,
  error: null,
};

export const selectorName = 'CarefieCampaigns_CarefieCampaignsCampaignsCreate';
export const getCarefieCampaignsCampaignsCreateStateSelector = createFeatureSelector<CarefieCampaignsCampaignsCreateState>(selectorName);

export function CarefieCampaignsCampaignsCreateReducer(
  state: CarefieCampaignsCampaignsCreateState = initialCarefieCampaignsCampaignsCreateState,
  action: actions.CarefieCampaignsCampaignsCreateAction): CarefieCampaignsCampaignsCreateState {
  switch (action.type) {
    case actions.Actions.START: return {...state, loading: true, error: null};
    case actions.Actions.SUCCESS: return {...state, data: action.payload, loading: false};
    case actions.Actions.ERROR: return {...state, error: action.payload, loading: false};
    default: return state;
  }
}
