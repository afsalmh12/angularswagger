/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

export interface User {
  readonly id?: number;
  /** Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only. */
  username: string;
  password: string;
  first_name?: string;
  last_name?: string;
  /** format: uri */
  readonly avatar?: string;
  /** format: date */
  dob?: string;
  gender?: string;
  address?: string;
  city?: string;
  state?: string;
  country?: string;
  postal_code?: string;
  is_carefie_newsletter_subscribed?: boolean;
  is_org_allowed_to_contact?: boolean;
  is_donors_allowed_to_send_friends_request?: boolean;
  is_allowed_to_display_carefies_on_profile_page?: boolean;
  is_campaign_updates_subscribed?: boolean;
  is_receive_thank_you_message?: boolean;
}
