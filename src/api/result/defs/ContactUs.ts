/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

export interface ContactUs {
  name: string;
  /** format: email */
  email: string;
  subject: string;
  message: string;
}
