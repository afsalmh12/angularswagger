/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

export interface Faq {
  readonly id?: number;
  question: string;
  answer: string;
}
