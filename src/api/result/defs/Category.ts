/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import * as __model from '../model';

export interface Category {
  readonly id?: number;
  category: string;
  sub_category: __model.SubCategory[];
}
