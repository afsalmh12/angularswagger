/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

export interface BankDocumentRead {
  readonly id?: number;
  /** format: uri */
  readonly file?: string;
  file_name: string;
}
