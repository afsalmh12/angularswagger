/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

export interface ResetPassword {
  token: string;
  /** format: email */
  email: string;
  new_password: string;
}
