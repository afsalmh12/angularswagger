/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

export interface CampaignCategory {
  readonly id?: number;
  category: string;
  /** format: uri */
  readonly icon?: string;
}
