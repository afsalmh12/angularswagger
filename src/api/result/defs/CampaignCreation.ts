/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

export interface CampaignCreation {
  readonly id?: number;
  title?: string;
  category: number;
  /** format: decimal */
  goal?: string;
  goal_type?: string;
  duration_days?: number;
  org_name?: string;
  org_street_address?: string;
  org_city?: string;
  org_state?: string;
  org_country?: string;
  org_postal_code?: string;
  org_country_code?: string;
  org_telephone_no?: string;
  org_website?: string;
  org_email_address?: string;
  org_contact_person_title?: string;
  org_contact_person_first_name?: string;
  org_contact_person_last_name?: string;
  /** format: date */
  org_registration_date?: string;
  org_registration_no?: string;
  org_EIN?: string;
  org_mission_statement?: string;
  org_principal_officer_title?: string;
  org_principal_officer_first_name?: string;
  org_principal_officer_last_name?: string;
  is_shipping_address_same_org_address?: boolean;
  shipping_street_address?: string;
  shipping_city?: string;
  shipping_state?: string;
  shipping_country?: string;
  shipping_postal_code?: string;
  shipping_country_code?: string;
  shipping_telephone_no?: string;
  shipping_contact_person_title?: string;
  shipping_contact_person_first_name?: string;
  shipping_contact_person_last_name?: string;
  summary?: string;
  introduction?: string;
  describe_your_problem?: string;
  how_make_things_better?: string;
  video_url?: string;
  /** format: uri */
  readonly banner_image?: string;
  bank_name?: string;
  bank_account_holder_name?: string;
  bank_account_no?: string;
  bank_BIC_or_SWIFT_code?: string;
  bank_national_clearing_code?: string;
  bank_branch_or_sort_code?: string;
  custom_campaign_url?: string;
  is_regular_giving_for_my_campaign_enabled?: boolean;
  how_donation_will_make_impact?: string;
  is_standard_thanks_msg_used?: boolean;
  custom_thanks_msg?: string;
}
