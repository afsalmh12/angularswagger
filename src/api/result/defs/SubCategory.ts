/* tslint:disable:max-line-length */
/**
 * Carefie API
 * v1
 * develop.carefie.com/api/v1
 */

import * as __model from '../model';

export interface SubCategory {
  readonly id?: number;
  sub_category: string;
  faq: __model.Faq[];
}
